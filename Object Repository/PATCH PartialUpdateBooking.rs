<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>PATCH PartialUpdateBooking</name>
   <tag></tag>
   <elementGuidId>df541810-0a21-4b91-b4d9-6a7e65f20cde</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;firstname\&quot; : \&quot;${firstname}\&quot;,\n    \&quot;lastname\&quot; : \&quot;${lastname}\&quot;,\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>d24fee91-0664-4a90-91d9-14198610bc57</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>539405a3-9bd5-423f-94de-6f32b2989866</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Cookie</name>
      <type>Main</type>
      <value>token=${GlobalVariable.token}</value>
      <webElementGuid>6e7d2853-3c0a-4a48-bdf2-f5033c3a4d2f</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.2</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PATCH</restRequestMethod>
   <restUrl>${GlobalVariable.URL}/booking/${id}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.createdId</defaultValue>
      <description></description>
      <id>e86406cb-c072-4baa-bb11-0deb9904c6e1</id>
      <masked>false</masked>
      <name>id</name>
   </variables>
   <variables>
      <defaultValue>'Nama'</defaultValue>
      <description></description>
      <id>c4678ae0-3868-407c-b4da-0301144d028c</id>
      <masked>false</masked>
      <name>firstname</name>
   </variables>
   <variables>
      <defaultValue>'Saya'</defaultValue>
      <description></description>
      <id>e48788fa-8c18-44c7-81da-84bccc12ae0f</id>
      <masked>false</masked>
      <name>lastname</name>
   </variables>
   <variables>
      <defaultValue>11111</defaultValue>
      <description></description>
      <id>6c627a36-feea-4680-8fc9-4eefedbe7a12</id>
      <masked>false</masked>
      <name>totalprice</name>
   </variables>
   <variables>
      <defaultValue>'2020-11-02'</defaultValue>
      <description></description>
      <id>8f9c4ee9-cc9b-488e-956c-e799c54f307a</id>
      <masked>false</masked>
      <name>checkin</name>
   </variables>
   <variables>
      <defaultValue>'2020-11-11'</defaultValue>
      <description></description>
      <id>0b84464c-1d2a-4f80-9b72-b28f51420fc5</id>
      <masked>false</masked>
      <name>checkout</name>
   </variables>
   <variables>
      <defaultValue>'Need Somebody'</defaultValue>
      <description></description>
      <id>a5ccb4b8-b271-4072-8d5f-2c43d9c155d7</id>
      <masked>false</masked>
      <name>additionalneeds</name>
   </variables>
   <variables>
      <defaultValue>true</defaultValue>
      <description></description>
      <id>dbacefac-915a-49ea-ad2b-2e1409563333</id>
      <masked>false</masked>
      <name>depositpaid</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)

def variables = request.getVariables()
def firstname = variables.get('firstname')
def lastname = variables.get('lastname')

WS.verifyElementPropertyValue(response, 'firstname', firstname)
WS.verifyElementPropertyValue(response, 'lastname', lastname)

</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
